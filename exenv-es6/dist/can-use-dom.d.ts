/**
 * Checks if the DOM is available to access and use
 */
export declare function canUseDOM(): boolean;
