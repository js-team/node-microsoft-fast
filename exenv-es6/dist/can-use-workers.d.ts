/**
 * Checks if Web Workers are available
 */
export declare function canUseWorkers(): boolean;
