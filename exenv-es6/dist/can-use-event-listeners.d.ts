/**
 * Checks if Event Listeners are available for use
 */
export declare function canUseEventListeners(): boolean;
