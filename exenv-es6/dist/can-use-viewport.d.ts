/**
 * Checks if viewport is available for use
 */
export declare function canUseViewport(): boolean;
