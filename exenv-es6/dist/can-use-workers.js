/**
 * Checks if Web Workers are available
 */
export function canUseWorkers() {
    return typeof Worker !== "undefined";
}
