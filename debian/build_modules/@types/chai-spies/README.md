# Installation
> `npm install --save @types/chai-spies`

# Summary
This package contains type definitions for chai-spies (https://github.com/chaijs/chai-spies).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/chai-spies.

### Additional Details
 * Last updated: Thu, 03 Dec 2020 12:42:26 GMT
 * Dependencies: [@types/chai](https://npmjs.com/package/@types/chai)
 * Global values: `spies`

# Credits
These definitions were written by [Ilya Kuznetsov](https://github.com/kuzn-ilya), [Harm van der Werf](https://github.com/harm-less), and [Jouni Suorsa](https://github.com/jounisuo).
